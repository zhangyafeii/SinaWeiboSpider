## SinaWeiboSpider

该项目爬取的数据字段说明，请移步:[数据字段说明与示例](./data_stracture.md)

### 项目说明

|         爬虫名称         |           功能           |
| :----------------------: | :----------------------: |
|       sina_account       |       用户信息下载       |
| sina_account_fans_follow | 用户粉丝和关注者数据下载 |
|    sina_account_weibo    |     用户微博信息下载     |
|    sina_weibo_comment    |       微博评论下载       |
|       sina_search        |     微博搜索信息下载     |

### 使用说明

```
git clone git@gitee.com:zhangyafeii/SinaWeiboSpider.git
cd SinaWeiboSpider
pip install -r requirements.txt
```

### 购买账号

> 小号购买地址: <http://www.wbxiaohao.com/> 更多小号购买的网站参考[这里](https://github.com/CUHKSZ-TQL/WeiboSpider_SentimentAnalysis/issues/2#issuecomment-505842345).
>
> 需要购买**绑号无验证码类型的微博小号**（重点！）
>
> 将购买的账号复制到sina/account_build/account.txt中，格式与account_sample.txt保持一致

### 构建账号池

```
python SinaWeiboSpider/account_build/login.py
```

- 运行结果

![](images/account_build.png)

![](images/account_build2.png)

![](images/account.png)

### 运行爬虫

> python main.py  # 通过修改main文件中的爬虫名称指定运行爬虫

具体可根据具体需求修改代码