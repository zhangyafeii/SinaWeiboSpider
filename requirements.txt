Scrapy==1.5.0
scrapy_redis==0.6.8
lxml==4.0.0
pymongo==3.7.0
selenium==3.14.0
Twisted==18.4.0
redis==2.10.6
