# -*- coding: utf-8 -*-

"""
Datetime: 2020/03/24
Author: Zhang Yafei
Description: 
"""
import datetime
import os
import time

num = 1
print('第一次执行')
os.system('python main.py')
start_time = time.strftime('%Y-%m-%d %X')
start_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
while True:
    now_time = time.strftime('%Y-%m-%d %X')
    now_time = datetime.datetime.strptime(now_time, "%Y-%m-%d %H:%M:%S")
    t1 = now_time - start_time
    if t1.seconds == 200:
        num += 1
        start_time = time.strftime('%Y-%m-%d %X')
        start_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
        os.system('python main.py')
        print(f'第{num}次执行')
