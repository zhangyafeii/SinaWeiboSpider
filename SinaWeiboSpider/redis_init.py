# -*- coding: utf-8 -*-

"""
Datetime: 2020/03/02
Author: Zhang Yafei
Description: 
"""
from redis import Redis, ConnectionPool

# 连接池
pool = ConnectionPool(host='127.0.0.1', port=6379)
conn = Redis(connection_pool=pool)


if __name__ == '__main__':
    # conn.delete('weibo_search:dupefilter')
    print(conn.smembers('weibo_search:dupefilter'))
    print(len(conn.smembers('weibo_search:dupefilter')))
    print(conn.keys())
