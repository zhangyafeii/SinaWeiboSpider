# -*- coding: utf-8 -*-

"""
Datetime: 2020/02/29
Author: Zhang Yafei
Description: 
"""
import pymongo
from pymongo.errors import DuplicateKeyError
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import sys
sys.path.append(os.getcwd())

from SinaWeiboSpider.settings import MONGO_URI, DB_NAME


class WeiboLogin():
    def __init__(self, username, password, headless=False):
        self.url = 'https://passport.weibo.cn/signin/login?entry=mweibo&r=https://weibo.cn/'
        if username not in {'15735177116', 'zangwu91047@163.com', 'irushen4928z@163.com'}:
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)
        else:
            self.driver = webdriver.Chrome()
        self.driver.set_window_size(1050, 840)
        self.wait = WebDriverWait(self.driver, 20)
        self.username = username
        self.password = password

    def open(self):
        """
        打开网页输入用户名密码并点击
        :return: None
        """
        self.driver.get(self.url)
        username = self.wait.until(EC.presence_of_element_located((By.ID, 'loginName')))
        password = self.wait.until(EC.presence_of_element_located((By.ID, 'loginPassword')))
        submit = self.wait.until(EC.element_to_be_clickable((By.ID, 'loginAction')))
        username.send_keys(self.username)
        password.send_keys(self.password)
        submit.click()
        # vaild_btn = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'geetest_btn')))
        # vaild_btn.click()

    def run(self):
        """
        破解入口
        :return:
        """
        self.open()
        if self.username in {'15735177116', 'zangwu91047@163.com', 'irushen4928z@163.com'}:
            WebDriverWait(self.driver, 30).until(
                EC.title_is('微博')
            )
        else:
            WebDriverWait(self.driver, 30).until(
                EC.title_is('我的首页')
            )
        cookies = self.driver.get_cookies()
        cookie = [item["name"] + "=" + item["value"] for item in cookies]
        cookie_str = '; '.join(item for item in cookie)
        self.driver.quit()
        return cookie_str


if __name__ == '__main__':
    # 在目录中新建一个account.txt文件，格式需要与account_sample.txt相同
    # 其实就是把www.xiaohao.shop买的账号复制到新建的account.txt文件中
    file_path = 'SinaWeiboSpider/account_build/account.txt'
    with open(file_path, 'r') as f:
        lines = f.readlines()
    mongo_client = pymongo.MongoClient(MONGO_URI)
    collection = mongo_client[DB_NAME]["account"]
    for line in lines:
        line = line.strip()
        username = line.split('----')[0]
        password = line.split('----')[1]
        print('=' * 10 + username + '=' * 10)
        try:
            cookie_str = WeiboLogin(username, password, headless=True).run()
        except Exception as e:
            print(e)
            continue
        # print('获取cookie成功')
        print('Cookie:', cookie_str)
        try:
            collection.insert(
                {"_id": username, "password": password, "cookie": cookie_str, "status": "success"})
            print('插入cookie成功')
        except DuplicateKeyError as e:
            collection.find_one_and_update({'_id': username}, {'$set': {'cookie': cookie_str, "status": "success"}})
            print('插入cookie成功')
        except Exception as e:
            print(e)
