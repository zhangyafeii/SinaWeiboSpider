# -*- coding: utf-8 -*-

"""
Datetime: 2020/03/02
Author: Zhang Yafei
Description: 
"""
import datetime
import os
import sys
from scrapy.cmdline import execute
import time


sys.path.append(os.path.dirname(__file__))


def run(job):
    execute(['scrapy', 'crawl', job])
    # execute(['scrapy', 'crawl', job, "--nolog"])


if __name__ == '__main__':
    # run(job="sina_account")
    # run(job="sina_account_fans_follow")
    # run(job="sina_account_weibo")
    run(job="sina_weibo_comment")
    # run(job="sina_search_weibo")

